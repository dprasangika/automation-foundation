﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace UnitTestProject
{
	[TestClass]
	public class LoginTest
	{
		private IWebDriver driver;

		[TestInitialize]
		public void BeforeEachTest()
		{
			driver = new ChromeDriver();
			driver.Url = "http://10.9.1.83/hubble/Login";

		}

		[TestCleanup]
		public void AfterEachTest()
		{
			driver.Close();
		}


		[TestMethod]
		public void Login()
		{	
			LoginPage loginPage = new LoginPage(driver);
//			loginPage.navigate();
			loginPage.Login("Humayra.Hanif@gohubble.com", "hubble");

//			Login("Humayra.Hanif@gohubble.com", "hubble");

			var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
			var hubElement = wait.Until(d => d.FindElement(By.ClassName("DEG1USC-w-k")));
			Assert.AreEqual("HUB", hubElement.Text);
		}


		[TestMethod]
		public void Invalid_Login_Test()
		{
			Login("fake@gohubble.com", "bad password");

			var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
			var errors = wait.Until(d => d.FindElement(By.ClassName("errors")));
			Assert.AreEqual("Invalid username or password.", errors.Text);
		}

		private void Login(string email, string pwd)
		{
			IWebElement username = driver.FindElement(By.Id("Username"));
			username.SendKeys(email);
			var password = driver.FindElement(By.Id("Password"));
			password.SendKeys(pwd);
			var signIn = driver.FindElement(By.ClassName("submit"));
			signIn.Click();
		}

	}
}

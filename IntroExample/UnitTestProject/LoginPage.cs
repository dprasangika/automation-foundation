﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace UnitTestProject
{ 

	public class LoginPage
	{
		private IWebDriver _seleniumDriver;

		public LoginPage(IWebDriver seleniumDriver)
		{
			_seleniumDriver = seleniumDriver;

		}

		public void Login(string email, string pwd)
		{
			IWebElement username = _seleniumDriver.FindElement(By.Id("Username"));
			username.SendKeys(email);
			var password = _seleniumDriver.FindElement(By.Id("Password"));
			password.SendKeys(pwd);
			var signIn = _seleniumDriver.FindElement(By.ClassName("submit"));
			signIn.Click();
		}
	}
}
